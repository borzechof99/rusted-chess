https://git.imp.fu-berlin.de/borzechof99/rusted-chess

Basisfiguren
- alle haben Grundfähigkeit
- alle haben Lebenspunkte

Turm:

- Gefangenenturm: assimiliert stats des Gefangenen, der kann wieder befreit werden

- Verbündetenturm:  kann von verbündeten verstärkt werden, wenn Turm stirbt, stirbt auch Verbündeter

Bischoff - wird 1x? wiederbelebt nach x Zeit

Bauer - Können Bauernhäuser (1x1) bauen

Dame - Versteinert direkt sichtbare Gegner _oder_ kann einmal pro Spiel mit einem gegnerischen Bauern tauschen

Springer - kann über Rand springen, Flächenschaden

König - Kann sich unter Bauern verstecken


Schachregeln:

- king of the hill?
- immer mögliche Züge und Angriffe zeigen, Eventlog führen
- Drei Häuser sind ein Dorf -> Heileffekt drumherum, neue Figuren generieren
- Eventuell verschiedene Völker?

Angreifen:

- Drei Felder vor Figur, eine davon aussuchen (außer Springer)



Karten:

- mit Verstärkungszaubern
- Gift - verstärkt Angriff, Damage über Zeit
- Heilung: Heile x Punkte
- "Das Horn" - Doppelter Schaden für x Runden
- Liebeszauber:

    - erbt stats wenn geliebter tot

    - kann erst an anderen Schaden machen wenn Rache vollführt

    - nur Bauern?

    - wenn König fremd geht, läuft Dame über



Zug besteht aus:
    - Aktion: Angriff
    - Bonusaktion: Zauber wirken
    - Bewegung: Laufen
    - passive: Grundfähigkeit
    
Ziel des Spiels:
    - gegnerischen König töten (FETTER Liebesbalken)
        


---------------------------------------------
Umsetzung:
    
Konsolendarstellung?

-visueller Output
-Regeln

Kampagne.. Nein!




--------------------------------------------------
Ressourcen:

https://crates.io/crates/miniquad

https://crates.io/crates/coffee

https://crates.io/crates/quicksilver

https://crates.io/crates/tetra
